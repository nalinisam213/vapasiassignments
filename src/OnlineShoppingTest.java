import java.util.ArrayList;

public class OnlineShoppingTest {

    public static void main(String[] args) {


        Onlineshopping owner = new ShopOwner();

        ArrayList<ProductDetails> brandDetails = new ArrayList<>();

        brandDetails.add(new ProductDetails("Kurthis", 4, 500));
        brandDetails.add(new ProductDetails("Lehangas", 4, 200));
        brandDetails.add(new ProductDetails("Shirts", 2, 500));
        brandDetails.add(new ProductDetails("Jeans", 3, 400));

        // Add the brands of clothes.
        owner.AddProductDetails("Biba", brandDetails);
        owner.AddProductDetails("Zara", brandDetails);
        owner.AddProductDetails("Levis", brandDetails);

        ///Users/vapasi/IdeaProjects/vapasi/OnlineShopping

        // Show brands information
        owner.ShowProductDetails();

        Onlineshopping customer = new Customer();
        //add the items to the cart
        brandDetails.clear();
        brandDetails.add(new ProductDetails("Kurthis", 2, 500));
        brandDetails.add(new ProductDetails("Lehangas", 2, 200));

        // Add the brands of clothes.
        customer.AddProductDetails("Biba", brandDetails);
        customer.AddProductDetails("Zara", brandDetails);

        //show the items in the cart
        customer.ShowProductDetails();


    }
}
