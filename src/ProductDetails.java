class ProductDetails {
    protected String name;
    protected int quantity;
    protected double unitprice;

    protected ProductDetails(String name, int quantity, double unitprice) {
        super();
        this.name = name;
        this.quantity = quantity;
        this.unitprice = unitprice;
    }

}
