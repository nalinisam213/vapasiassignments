import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShopOwner implements Onlineshopping {
    private Map<String, ArrayList<ProductDetails>> productlist;

    public ShopOwner() {
        productlist = new HashMap<>();
    }

    @Override
    public void AddProductDetails(String brandname, ArrayList<ProductDetails> branddetails) {
        //Owner adding new Products
        productlist.put(brandname, branddetails);
    }

    @Override
    public void ShowProductDetails() {
        //Display of Products to owner
        System.out.println("Available Stock Details:");
        for (Map.Entry mapElement : productlist.entrySet()) {
            String key = (String) mapElement.getKey();
            ArrayList<ProductDetails> branddetails = productlist.get(key);
            System.out.println('\n' + "BrandName: " + key);
            for (ProductDetails stock : branddetails)
                System.out.println(stock.name + " " + stock.quantity + " " + stock.unitprice);
        }
    }
}
