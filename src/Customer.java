import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Customer implements Onlineshopping {
    private Map<String, ArrayList<ProductDetails>> productlist;
    public Customer() {
        productlist = new HashMap<>();
    }

    @Override
    public void AddProductDetails(String brandname, ArrayList<ProductDetails> branddetails) {
        //Selecting Items added to Cart
        productlist.put(brandname, branddetails);
    }

    @Override
    public void ShowProductDetails() {
        //Display of cart details
        System.out.println('\n' + "Cart Details:");
        for (Map.Entry mapElement : productlist.entrySet()) {
            String key = (String) mapElement.getKey();
            ArrayList<ProductDetails> branddetails;
            branddetails = productlist.get(key);
            System.out.println('\n' + "BrandName: " + key);
            for (ProductDetails stock : branddetails)
                System.out.println(stock.name + " " + stock.quantity + " " + stock.unitprice);
        }
    }
}
